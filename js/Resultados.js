
 
// definimos el array que contendra los respuestados de la encuesta
var resultadoEncuesta=[];
 
// definimos la variable que contiene el numero de preguntas
var totalPreguntas=5;
 
// inicializamos el array de los resultados de la forma:
// resultadoEncuesta["p1"]=[0,0]
// resultadoEncuesta["p2"]=[0,0]
// ...
inicializarArrayResultados();
 
// variable que contiene el numero de encuestas realizadas
var totalEncuestas=0;
 
/**
 * Funcion que se ejecuta cada vez que se pulsa sobre el boton "enviar"
 */
function enviar(e) {
 
    // obtenemos todos los radio seleccionados
    var preguntas=document.querySelectorAll("input[type=radio]:checked");
 
    // si estan todos seleccionados...
    if(preguntas.length==totalPreguntas) {
 
        totalEncuestas++;
        document.getElementById("error").innerHTML="";
 
        // recorremos cada una de las respuestas
        preguntas.forEach(function(pregunta) {
 
            // guardamos en un array bidimensional la respuesta
            resultadoEncuesta[pregunta.name][pregunta.value]++;
 
            // desmarcamos el check
            pregunta.checked=false;
        });
        mostrarResultado();
    }else{
        document.getElementById("error").innerHTML="Selecciona todos los valores...";
    }
 
    // cancelamos el evento para que no continue
    e.preventDefault();
}
 
/**
 * Funcion para inidializar el array bidimensional
 */
function inicializarArrayResultados() {
    for(var i=1;i<=totalPreguntas;i++) {
        resultadoEncuesta["p"+i]=[0,0];
    }
}
 
/**
 * Simple funcion que muestra los resultados en cada votacion
 */
function mostrarResultado() {
    resultado="";
    resultado+="<h3>De un total de "+totalEncuestas+" encuestados...</h3>";
    for(var i=1;i<=totalPreguntas;i++) {
        resultado+="<div>pregunta "+i+" - Si: "+resultadoEncuesta["p"+i][0]+" No: "+resultadoEncuesta["p"+i][1]+"</div>";
    }
    document.getElementById("resultado").innerHTML=resultado;
}
 

var contador;
function calificar(item){
    console.log(item);
    contador=item.id[0];// captura el primer caracter
    let nombre = item.id.substring(1);// 4 "estrella" captura  todo menos el primer caracter
    for(let i=0;i<5;i++){
        //i-0
        //i-1
        //i-2
        //i-3
        //if(4<2)
        if(i<contador){
            //primera vez 0+1 = 1estrella;
            //segunda vez 1+1 = 2estrella;
            //tercera vez 2+1 = 3estrella;
            //cuarta  vez  3+1 = 4estrella;
            document.getElementById((i+1)+nombre).style.color="orange";
        }else{
            document.getElementById((i+1)+nombre).style.color="black";        }
        }
    }

    function validar() {
    
    
            
        alert("Encuesta enviada");  
    
    }
