const app = {
  data() {
    return {
      preguntas: {
        "pregunta1":null,
        "pregunta2": null,
        "pregunta3": null,
        "pregunta4": null,
        "pregunta5": null,
        "pregunta6": null    
      }
    }
  },
  methods:{
    enviarEncuesta(){
      //Validamos que halla escojidos las preguntas
      if(!this.preguntas.pregunta1){
        alert("Escoja la pregunta 1!");
        return;
      }
      if(!this.preguntas.pregunta2){
        alert("Escoja la pregunta 2!");
        return;
      }
      if(!this.preguntas.pregunta3){
        alert("Escoja la pregunta 3!");
        return;
      }
      if(!this.preguntas.pregunta4){
        alert("Escoja la pregunta 4!");
        return;
      }
      if(!this.preguntas.pregunta5){
        alert("Escoja la pregunta 5!");
        return;
      }
      if(!this.preguntas.pregunta6){
        alert("Escoja la pregunta 6!");
        return;
      }
      //Mostrarmos la alerta si se envios exitosamente
      alert('Encuesta enviada!');
      //Imprimimos por consola las preguntas
      console.log(this.preguntas);
      //Establecemos las preguntas a su valor predeterminado
      this.preguntas = {
        "pregunta1":null,
        "pregunta2": null,
        "pregunta3": null,
        "pregunta4": null,
        "pregunta5": null,
        "pregunta6": null    
      }
    }
  }
}

//Montamos la app de vue
Vue.createApp(app).mount('#app')