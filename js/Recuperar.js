function sendPasswordReset() {
    // [START auth_send_password_reset]
    var email = document.getElementById("email").value;
    firebase.auth().languageCode = 'it';
    firebase.auth().useDeviceLanguage();
    firebase.auth().sendPasswordResetEmail(email)
        .then(() => {
            alert("se envio un mensaje para restablecer su contraseña")
        })
        .catch((error) => {
            var errorCode = error.code;
            var errorMessage = error.message;
            alert("error, ingrese un email valido.")
        });
}