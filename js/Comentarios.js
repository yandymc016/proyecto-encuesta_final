/* EXPRESIONES REGULARES **************/
const expresiones_nombre = /^[a-zA-Z]{4,20}$/;
const expresiones_comentario = /^[a-zA-Z ]{4,300}$/;
const expresiones_correo = /^\w+@\w+\.+[aZ-zA]{2,3}$/;


/* VALIDACION DE REGISTRO Y QUE NO CARGUE LA PAGINA*/
const validarComentarioo = document.getElementById("formulario-comentarios");
validarComentarioo.addEventListener("submit", async(e) => {
    var nombres = document.getElementById("nombres").value;
    var comentario = document.getElementById("comentario").value;
    var correo = document.getElementById("correo").value;
   

    if (nombres == "" || comentario == "" || correo == "" ) {
        e.preventDefault();
        alert("Todos los campos son obligatorios");
        return false;
    } else if (!expresiones_nombre.test(nombres)) {
        e.preventDefault();
        alert("El nombre debe contener entre 4 y 20 caracteres");
        return false;
    } else if (!expresiones_comentario.test(comentario)) {
        e.preventDefault();
        alert("El comentario no debe pasar los 300 caracteres");
        return false;
    } else if (!expresiones_correo.test(correo)) {
        e.preventDefault();
        alert("El correo no es valido");
        return false;
   
    
    } else {
        const enviar = (nombres,comentario, correo) => db.collection('Comentarios').doc().set({
            nombres,
            comentario,
            correo,
           
        });
        e.preventDefault();

        const nombres = validarComentarioo["nombres"].value;
        const comentario = validarComentarioo["comentario"].value;
        const correo = validarComentarioo["correo"].value;

       
        //const email = registrarse["correo"].value;
        //const password = registrarse["password"].value;
        alert("Comentario enviado exitosamente");

        await enviar(nombres, comentario, correo);
        
        validarComentarioo.reset();
                console.log(enviar);

       

    }
});