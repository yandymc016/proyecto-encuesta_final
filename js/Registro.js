/* EXPRESIONES REGULARES **************/
const expresiones_nombre = /^[a-zA-Z]{4,20}$/;
const expresiones_apellido = /^[a-zA-Z]{4,20}$/;
const expresiones_correo = /^\w+@\w+\.+[aZ-zA]{2,3}$/;
const expresiones_contraseña = /^.{4,12}$/;
const expresiones_contraseña2 = /^.{4,12}$/;

/* VALIDACION DE REGISTRO Y QUE NO CARGUE LA PAGINA*/
const validarRegistro = document.getElementById("formulario-registro");
validarRegistro.addEventListener("submit", async(e) => {
    var nombres = document.getElementById("nombres").value;
    var apellidos = document.getElementById("apellidos").value;
    var correo = document.getElementById("correo").value;
    var password = document.getElementById("password").value;
    var password2 = document.getElementById("password2").value;

    if (nombres == "" || apellidos == "" || correo == "" || password == "" || password2 == "") {
        e.preventDefault();
        alert("Todos los campos son obligatorios");
        return false;
    } else if (!expresiones_nombre.test(nombres)) {
        e.preventDefault();
        alert("El nombre debe contener entre 4 y 20 caracteres");
        return false;
    } else if (!expresiones_apellido.test(apellidos)) {
        e.preventDefault();
        alert("El apellido debe contener entre 4 y 20 caracteres");
        return false;
    } else if (!expresiones_correo.test(correo)) {
        e.preventDefault();
        alert("El correo no es valido");
        return false;
    } else if (!expresiones_contraseña.test(password)) {
        e.preventDefault();
        alert("La contraseña debe contener entre 4 y 12 caracteres");
        return false;
    } else if (password != password2) {
        e.preventDefault();
        alert("Las contraseñas no coinciden");
        return false;
    } else {
        const enviar = (nombres, apellidos, correo, password, password2) => db.collection('UsuariosRegistrados').doc().set({
            nombres,
            apellidos,
            correo,
            password,
            password2
        });
        e.preventDefault();

        const nombres = validarRegistro["nombres"].value;
        const apellidos = validarRegistro["apellidos"].value;
        const correo = validarRegistro["correo"].value;

        const password = validarRegistro["password"].value;
        const password2 = validarRegistro["password2"].value;
        //const email = registrarse["correo"].value;
        //const password = registrarse["password"].value;
        alert("Te has registrado exitosamente");

        await enviar(nombres, apellidos, correo, password, password2);
        // AUTENTICACION DE USUARIOS
        console.log(enviar);

        auth
            .createUserWithEmailAndPassword(correo, password)
            .then((userCredential) => {
                // RESETEAR FORMULARIO

                // registrarse.reset();


            });
        // return true;

    }
});